package com.nur.graphqlsample.service;

import com.nur.graphqlsample.dao.entity.User;
import com.nur.graphqlsample.dao.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Transactional
    public User createUser(final String firstName, final String lastName, final int age) {
        final User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAge(age);
        return this.userRepository.save(user);
    }
    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    @Transactional
    public Boolean deleteUser(final int id) {
        userRepository.deleteById(id);
        return true;
    }
    @Transactional
    public User updateUser(int id, String firstName, String lastName, int age) {
        final User user = new User();
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAge(age);
        return userRepository.saveAndFlush(user);
    }
    @Transactional
    public List<User> getUserByFirstName(String firstName) {
        return userRepository.findByFirstNameContainsIgnoreCase(firstName);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUser(final int id) {
        return this.userRepository.findById(id);
    }

    @Transactional
    public List<User> getAll(int minAge, int maxAge){
        if (minAge==0){
            return userRepository.findUserByAgeIsLessThanEqual(maxAge);
        }
        else if(maxAge==0){
             return userRepository.findUserByAgeIsGreaterThanEqual(minAge);
        }
        else {
            return userRepository.findByAgeBetween(minAge, maxAge);
        }
    }
}

