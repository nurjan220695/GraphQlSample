package com.nur.graphqlsample.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.nur.graphqlsample.dao.entity.User;
import com.nur.graphqlsample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMutationResolver implements GraphQLMutationResolver {
    @Autowired
    private UserService userService;
    public User createUser(final String firstName, final String lastName, final int age) {
        return this.userService.createUser(firstName, lastName, age);
    }
    public Boolean deleteUser(int id) {
            this.userService.deleteUser(id);
            return true;
    }
    public User updateUser(int id, String firstName, String lastName, int age) {
        return this.userService.updateUser(id,firstName,lastName,age);
    }
}