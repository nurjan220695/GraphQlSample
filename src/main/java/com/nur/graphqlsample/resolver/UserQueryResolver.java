package com.nur.graphqlsample.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.nur.graphqlsample.dao.entity.User;
import com.nur.graphqlsample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Component
public class UserQueryResolver implements GraphQLQueryResolver {
    @Autowired
    private UserService userService;

    public Optional<User> getUser(final int id) {
        return this.userService.getUser(id);
    }
    public  List<User> getAllUsers(String searchText) {
        if (searchText == null){
            return this.userService.getAllUsers();
        }
        else {
            return userService.getUserByFirstName(searchText);

        }
    }
    public List<User> getAll(int minAge,int maxAge){
        return userService.getAll(minAge,maxAge);
    }
}
