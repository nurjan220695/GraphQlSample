package com.nur.graphqlsample.dao.repository;

import com.nur.graphqlsample.dao.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
    List<User> findByFirstNameContainsIgnoreCase(String firstName);
    List<User> findByAgeBetween(int minAge, int maxAge);
    List<User> findUserByAgeIsGreaterThanEqual (int minAge);
    List<User> findUserByAgeIsLessThanEqual (int maxAge);

}
